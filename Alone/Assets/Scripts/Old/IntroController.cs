using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class IntroController : MonoBehaviour
{
    [SerializeField]
    private float m_Speed;

    [SerializeField]
    private float m_RotationSpeed;

    Vector3 m_Movement = Vector3.zero;
    private Rigidbody m_Rigidbody;

    MyInputMap m_InputMap;
    public MyInputMap InputMap=> m_InputMap;



    Animator m_Animator;
    private void Awake()
    {
        m_InputMap = new MyInputMap();
        m_InputMap.Land.Enable();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_Animator = GetComponent<Animator>();
        
        
    }

    void Update()
    {
        //rotate
        if (Input.GetKey(KeyCode.A))
            transform.Rotate(-Vector3.up * m_RotationSpeed * Time.deltaTime);
        if (Input.GetKey(KeyCode.D))
            transform.Rotate(Vector3.up * m_RotationSpeed * Time.deltaTime);

        m_Movement = Vector3.zero;

        if (Input.GetKey(KeyCode.W))
            m_Movement += transform.forward;
        if (Input.GetKey(KeyCode.S))
            m_Movement -= transform.forward;

        //strafe
        if (Input.GetKey(KeyCode.Q))
            m_Movement -= transform.right;
        if (Input.GetKey(KeyCode.E))
            m_Movement += transform.right;

        if (Input.GetKeyDown(KeyCode.W))
            m_Animator.Play("run");
        if (Input.GetKeyUp(KeyCode.W))
            m_Animator.Play("idle1");
        

        m_Movement.Normalize();

        //transform.position += m_Movement * m_Speed *Time.deltaTime;
    }

    private void FixedUpdate()
    {
        UpdateState(m_CurrentState);
        m_Rigidbody.MovePosition(transform.position + m_Movement * m_Speed * Time.fixedDeltaTime);
    }


    private float m_StateDeltaTime;
    private Coroutine m_ComboTimeCoroutine;
    private enum States { IDLE, PUNCH,JUMP, SHOOT,
        MOVEFW,MOVEBW,MOVERIGHT,MOVELEFT,
        STRAFERIGHT,STRAFELEFT }
    private States m_CurrentState;

    bool combo = false;

   
    private void InitState(States initState)
    {
        m_CurrentState = initState;
        m_StateDeltaTime = 0;

        switch (m_CurrentState)
        {
            case States.IDLE:
                m_Animator.Play("idle");
                break;
            case States.PUNCH:
                m_Animator.Play("punch");
                m_ComboTimeCoroutine = StartCoroutine(CorutineCombo(0.65f, 1.1f));
                break;
            case States.SHOOT:
                m_Animator.Play("shoot");
                break;
            case States.JUMP:
                break;
            case States.MOVEFW:
                break;
            case States.MOVEBW:
                break;
            case States.MOVERIGHT:
                break;
            case States.MOVELEFT:
                break;
            case States.STRAFERIGHT:

                break;
            case States.STRAFELEFT:

                break;
            default:
                break;
        }
    }


    private void UpdateState(States updateState)
    {
        m_StateDeltaTime += Time.deltaTime;

        switch (updateState)
        {
            case States.IDLE:
                if (Input.GetKeyDown(KeyCode.A))
                    ChangeState(States.PUNCH);

                break;
            case States.PUNCH:
                if (Input.GetKeyDown(KeyCode.Space) && combo)
                    ChangeState(States.SHOOT);

                else if (m_StateDeltaTime >= 1.2f)
                    ChangeState(States.IDLE);

                break;
            case States.SHOOT:
                if (m_StateDeltaTime >= 0.4f)
                    ChangeState(States.IDLE);
                break;
            case States.JUMP:
                break;
            case States.MOVEFW:
                break;
            case States.MOVEBW:
                break;
            case States.MOVERIGHT:
                break;
            case States.MOVELEFT:
                break;
            case States.STRAFERIGHT:
                break;
            case States.STRAFELEFT:
                break;
            default:
                break;
        }
    }

    private void ExitState(States exitState)
    {
        switch (exitState)
        {
            case States.IDLE:
                break;
            case States.PUNCH:
                StopCoroutine(m_ComboTimeCoroutine);
                break;
            case States.SHOOT:
                break;
            case States.JUMP:
                break;
            case States.MOVEFW:
                break;
            case States.MOVEBW:
                break;
            case States.MOVERIGHT:
                break;
            case States.MOVELEFT:
                break;
            case States.STRAFERIGHT:
                break;
            case States.STRAFELEFT:
                break;
            default:
                break;
        }
    }

    private IEnumerator CorutineCombo(float comboWindowStart, float comboWindowEnd)
    {
        combo = false;
        yield return new WaitForSeconds(comboWindowStart);
        combo = true;
        yield return new WaitForSeconds(comboWindowEnd);
        combo = false;
    }
    private void ChangeState(States newState)
    {
        if (newState == m_CurrentState)
            return;

        ExitState(m_CurrentState);
        InitState(newState);
    }


}
