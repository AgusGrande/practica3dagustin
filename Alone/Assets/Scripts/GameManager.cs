using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private static GameManager m_Instance;

    [SerializeField]
    private TMPro.TextMeshProUGUI textFinalUI;
    private TimeSpan timePlaying;
    private bool m_timerGoing;
    private float elapsedTime;

    public static GameManager Instance
    {
        get { return m_Instance; }
    }

    private void Awake()
    {
        if (m_Instance == null)
            m_Instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }

    private void Start()
    {
        textFinalUI.text = "00:00.00";
        m_timerGoing = false;
    }

    public void BeginTimer()
    {
        StopAllCoroutines();
        m_timerGoing = true;
        elapsedTime = 0f;
        StartCoroutine(UpdateTimer());
    }

    public void EndTimer()
    {
        m_timerGoing = false;
    }

    private IEnumerator UpdateTimer()
    {
        while (m_timerGoing)
        {
            elapsedTime += Time.deltaTime;
            timePlaying = TimeSpan.FromSeconds(elapsedTime);
            string timePlayingStr = "" + timePlaying.ToString("mm':'ss'.'ff");
            textFinalUI.text = timePlayingStr;
            yield return null;
        }
    }



}
