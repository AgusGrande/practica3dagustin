using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGame : MonoBehaviour
{
    /*
     * Simplemente llama a la funcion de Iniciar/Reiniciar el cronometro al pisar la Plataforma
     */

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            GameManager.Instance.BeginTimer();
            SoundManager.PlaySound("amongussound");
        }
        
            
        
    }
}
