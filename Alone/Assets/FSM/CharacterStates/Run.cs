using FiniteStateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Run : Movement
{
    public Run(FSM fSM, string movementAnimation, float movementSpeed)
      : base(fSM, movementAnimation, movementSpeed)
    {
    }

    public override void Init()
    {
        base.Init();
        m_PlayerBehaviour.m_InputMap.Land.Run.canceled += StopRunning;
        m_PlayerBehaviour.moveSpeed = m_MovementSpeed;
    }

    public override void Exit()
    {
        base.Exit();
        m_PlayerBehaviour.m_InputMap.Land.Run.canceled -= StopRunning;
    }

    private void StopRunning(InputAction.CallbackContext context)
    {
        m_FSM.ChangeState<Walk>();
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
        if (m_PlayerBehaviour.grounded
           && m_MovimentFinal == Vector3.zero)
            m_FSM.ChangeState<Idle>();

    }

}
