using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGame : MonoBehaviour
{
    /*
    * Simplemente llama a la funcion de Parar el cronometro al pisar la Plataforma
    */
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            GameManager.Instance.EndTimer();
            SoundManager.PlaySound("amongussound0");
        }
           
    }
}
