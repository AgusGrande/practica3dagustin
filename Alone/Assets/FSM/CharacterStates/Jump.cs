using FiniteStateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : Movement
{
    AGUSMOVEMENT m_PlayerBehaviour;
    string m_Animation;
    float jumpForce;


    public Jump(FSM fsm, float movementSpeed, string animation,float jumpForce) : base(fsm,animation, movementSpeed)
    {
        m_Animation = animation;
        this.jumpForce = jumpForce;
        m_PlayerBehaviour = m_FSM.Owner.GetComponent<AGUSMOVEMENT>();
    }
    public override void Init()
    {
        Debug.Log("Entering state: " + GetType());
        m_FSM.Owner.GetComponentInChildren<Animator>().Play(m_Animation);
        SoundManager.PlaySound("jumpsound");
        m_RigidBody.velocity = new Vector3(m_RigidBody.velocity.x, 0.01f, m_RigidBody.velocity.z);
        m_RigidBody.AddForce(m_FSM.Owner.transform.up * jumpForce, ForceMode.Impulse);
        m_PlayerBehaviour.UpdateReadyToJump();
        
    }
    public override void Exit()
    {
        base.Exit();
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
        if (m_PlayerBehaviour.grounded && m_PlayerBehaviour.GetComponent<Rigidbody>().velocity.y==0)
            m_FSM.ChangeState<Idle>();
        

    }
    
    


}
