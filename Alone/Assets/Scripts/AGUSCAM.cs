using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AGUSCAM : MonoBehaviour
{
    [Header("References")]
    public Transform orientation;
    public Transform player;
    public Transform playerObj;
    public Rigidbody rb;

    public AGUSMOVEMENT playerMIO;

    public float rotationSpeed;

    
    
    public GameObject ponercamera;


    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void Update()
    {
       
        // rotate orientation
        Vector3 viewDir = player.position - new Vector3(ponercamera.transform.position.x, player.position.y, ponercamera.transform.position.z);
        orientation.forward = viewDir.normalized;

        // roate player object
        Vector2 MO = playerMIO.m_InputMap.Land.Movement.ReadValue<Vector2>();
        Vector2 movementtmp = Vector2.zero;
        if (MO.x > 0.5f)// D
            movementtmp.x += 1;
        else if (MO.x < -0.5f) // A 
            movementtmp.x += -1;
        if (MO.y > 0.5f)// W
            movementtmp.y += 1;
        else if (MO.y < -0.5f)// S
            movementtmp.y += -1;

        
        Vector3 inputDir = orientation.forward * movementtmp.y + orientation.right * movementtmp.x;
        if (inputDir != Vector3.zero)
            playerObj.forward = Vector3.Slerp(playerObj.forward, inputDir.normalized, Time.deltaTime * rotationSpeed);
    }

   
}
