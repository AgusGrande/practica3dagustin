using FiniteStateMachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Walk : Movement
{
    public Walk(FSM fSM, string movementAnimation, float movementSpeed)
      : base(fSM, movementAnimation, movementSpeed)
    {
    }
    public override void Init()
    {
        base.Init();
        m_PlayerBehaviour.m_InputMap.Land.Run.performed += StartRunning;
        m_PlayerBehaviour.moveSpeed = m_MovementSpeed;
    }

    public override void Exit()
    {
        base.Exit();
        m_PlayerBehaviour.m_InputMap.Land.Run.performed -= StartRunning;
    }

    private void StartRunning(InputAction.CallbackContext context)
    {
        m_FSM.ChangeState<Run>();
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
        if (m_PlayerBehaviour.grounded
           && m_MovimentFinal == Vector3.zero)
            m_FSM.ChangeState<Idle>();

        bool JumpPressed = m_PlayerBehaviour.m_InputMap.Land.Running.ReadValue<float>() > 0.1f;
        if(JumpPressed)
            m_FSM.ChangeState<Run>();

    }
     


}
