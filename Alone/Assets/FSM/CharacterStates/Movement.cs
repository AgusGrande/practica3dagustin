using FiniteStateMachine;
using UnityEngine;
using UnityEngine.InputSystem;

public class Movement : State
{

    protected Rigidbody m_RigidBody;
    protected AGUSMOVEMENT m_PlayerBehaviour;
    string m_MovementAnimation;
    protected float m_MovementSpeed;

    protected Vector3 m_MovimentFinal = Vector3.zero;
    protected Vector2 MO;

    public Movement(FSM fSM, string movementAnimation, float movementSpeed)
        : base(fSM)
    {
        m_PlayerBehaviour = fSM.Owner.GetComponent<AGUSMOVEMENT>();
        m_RigidBody = fSM.Owner.GetComponent<Rigidbody>();
        m_MovementAnimation = movementAnimation;
        m_MovementSpeed = movementSpeed;
    }

    public override void Init()
    {
        base.Init();
        m_PlayerBehaviour = m_FSM.Owner.GetComponent<AGUSMOVEMENT>();
        m_PlayerBehaviour.m_InputMap.Land.Jump.performed += StartedJumping;

        m_FSM.Owner.GetComponentInChildren<Animator>().Play(m_MovementAnimation);
    }

    public override void Exit()
    {
        base.Exit();
        m_PlayerBehaviour.m_InputMap.Land.Jump.performed -= StartedJumping;
    }


    private void StartedJumping(InputAction.CallbackContext context)
    {
        if (m_PlayerBehaviour.readyToJump && m_PlayerBehaviour.grounded)
        {
            m_FSM.ChangeState<Jump>();
        }

        if(!m_PlayerBehaviour.grounded)
            Debug.Log("NOT GROUNDED");

        if(!m_PlayerBehaviour.readyToJump)
            Debug.Log("NOT READY TO JUMP");
        if(m_RigidBody.velocity.y!=0)
            Debug.Log("Velocity diferent de 0->"+m_RigidBody.velocity.y);
    }

    public override void FixedUpdate()
    {
        /* Velocidad a traves de addforce con un limitador
        m_RigidBody.AddForce(m_MovimentFinal.normalized * m_MovementSpeed *8, ForceMode.Force);
        Vector3 velocitat = new Vector3(m_RigidBody.velocity.x, 0, m_RigidBody.velocity.z);
        if (velocitat.magnitude > m_MovementSpeed)
        {
            Vector3 velocitatMax = velocitat.normalized * m_MovementSpeed;
            m_RigidBody.velocity = new Vector3(velocitatMax.x,m_RigidBody.velocity.y,velocitatMax.z);
        }
        */
        CheckMovementInput();

        m_PlayerBehaviour.GetComponent<Rigidbody>().velocity = m_MovimentFinal.normalized * m_PlayerBehaviour.moveSpeed + new Vector3(0, m_RigidBody.velocity.y, 0) ;

        if (!m_PlayerBehaviour.grounded && m_PlayerBehaviour.GetComponent<Rigidbody>().velocity.y < -0.2f)
        {
            //velocity.y<-0.2f para que al bajar una rampa no cambie
            m_FSM.ChangeState<Falling>();
            Debug.Log("MOVEMENT STATE SAYS: NOT GROUNDED CHANGING TO FALLING");
        }
        

    }

    protected void CheckMovementInput()
    {
        MO = m_PlayerBehaviour.m_InputMap.Land.Movement.ReadValue<Vector2>();
        m_MovimentFinal = Vector3.zero;
        if (MO.x > 0.5f)// D
            m_MovimentFinal += m_PlayerBehaviour.orientation.right;
        else if (MO.x < -0.5f) // A 
            m_MovimentFinal += -m_PlayerBehaviour.orientation.right;
        if (MO.y > 0.5f)// W
            m_MovimentFinal += m_PlayerBehaviour.orientation.forward;
        else if (MO.y < -0.5f)// S
            m_MovimentFinal += -m_PlayerBehaviour.orientation.forward;
    }


}