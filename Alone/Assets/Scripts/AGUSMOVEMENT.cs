using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.InputSystem;
using FiniteStateMachine;

public class AGUSMOVEMENT : MonoBehaviour
{
    [Header("Movement")]
    public float moveSpeed;
    public float walkSpeed;
    public float runSpeed;



    [Header("Jump")]
    public float jumpForce;
    public float jumpCooldown;
    public float airMultiplier;
    public bool readyToJump;

   
    [Header("Ground Check")]
    public float playerHeight;
    public LayerMask whatIsGround;
    public bool grounded=true;


    Animator m_animator;
    Rigidbody rb;
    FSM m_FSM;

    public MyInputMap m_InputMap;

    [Header("Player")]
    public Transform orientation;
    public Transform player;
    public GameObject m_PlayerOBJ;
    

    Vector3 moveDirection;




    private void Awake()
    {
        m_animator = GetComponentInChildren<Animator>();
        moveDirection = Vector3.zero;
        m_InputMap = new MyInputMap();
        m_InputMap.Land.Enable();
        rb = GetComponent<Rigidbody>();
        rb.freezeRotation = true;
        readyToJump = true;

        m_FSM = new FSM(gameObject);
        m_FSM.AddState(new Idle(m_FSM,"idle1"));
        m_FSM.AddState(new Jump(m_FSM,moveSpeed, "jump", jumpForce));
        m_FSM.AddState(new Walk(m_FSM,"walk",walkSpeed));
        m_FSM.AddState(new Falling(m_FSM,"fall",moveSpeed));
        m_FSM.AddState(new Run(m_FSM,"run",runSpeed));
        
        m_FSM.ChangeState<Idle>();

    }

    private void Update()
    {
        m_FSM.Update();
       
    }

    private void FixedUpdate()
    {
        m_FSM.FixedUpdate();
        grounded = Physics.Raycast(transform.position, Vector3.down, playerHeight * 0.5f + 0.3f, whatIsGround);

    }


    /*
    private void Update()
    {
        grounded = Physics.Raycast(transform.position, Vector3.down, playerHeight * 0.5f + 0.3f, whatIsGround);

        MyInput();
        moveDirection = orientation.forward * verticalInput + orientation.right * horizontalInput;
        SpeedControl();

        // handle drag
        if (grounded)
            rb.drag = groundDrag;
        else
            rb.drag = 0;

        Vector3 mirando = player.position - new Vector3(transform.position.x, player.position.y, transform.position.z);
        orientation.forward = mirando.normalized;

       // if (moveDirection != Vector3.zero)
            //playerObj.forward = Vector3.Slerp(playerObj.forward, inputDir.normalized, Time.deltaTime * rotationSpeed);
    }

    private void FixedUpdate()
    {   
        if (moveDirection!=Vector3.zero)
            MovePlayer();
    }

    private void MyInput()
    {
        horizontalInput = m_InputMap.Land.Movement.ReadValue<Vector2>().x;
        verticalInput = m_InputMap.Land.Movement.ReadValue<Vector2>().y;
    }

    private void MovePlayer()
    {
        // calculate movement direction
        

        // on ground
        if (grounded)
        {
            m_animator.Play("run");
            rb.AddForce(moveDirection.normalized * moveSpeed * 10f, ForceMode.Force);

        }
        // in air
        else if(!grounded)
            rb.AddForce(moveDirection.normalized * moveSpeed * 10f * airMultiplier, ForceMode.Force);
    }

    private void SpeedControl()
    {
        Vector3 flatVel = new Vector3(rb.velocity.x, 0f, rb.velocity.z);

        // limit velocity if needed
        if(flatVel.magnitude > moveSpeed)
        {
            Vector3 limitedVel = flatVel.normalized * moveSpeed;
            rb.velocity = new Vector3(limitedVel.x, rb.velocity.y, limitedVel.z);
        }
    }*/

    public void UpdateReadyToJump()
    {
        readyToJump = false;
        Invoke(nameof(ResetJump), jumpCooldown);
    }
    private void ResetJump()
    {
        readyToJump = true;
    }
   
}