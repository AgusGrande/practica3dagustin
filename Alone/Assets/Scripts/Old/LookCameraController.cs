using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookCameraController : MonoBehaviour
{
    [SerializeField]
    private GameObject[] m_Target;

    private int m_Index = 0;

    public int yyy=0;
    public float damping = 0;

    [SerializeField]
    private Vector3 m_Offset;

    private Vector3 velocity = Vector3.zero;

    void Update()
    {

        Vector3 tmp = m_Target[m_Index].transform.position + m_Target[m_Index].transform.forward * m_Offset.z + m_Target[m_Index].transform.up * m_Offset.y;
        transform.position = Vector3.SmoothDamp(transform.position,tmp, ref velocity, damping);


        transform.LookAt(m_Target[m_Index].transform.position+new Vector3(0, yyy, 0));

        if (Input.GetKeyDown(KeyCode.C))
            m_Index = (m_Index+1)%m_Target.Length;
    }
}
