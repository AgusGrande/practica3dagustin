using FiniteStateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Falling : Movement
{
    string m_MovementAnimation;
    
    public Falling(FSM fSM, string movementAnimation, float movementSpeed)
      : base(fSM, movementAnimation, movementSpeed)
    {
        this.m_MovementAnimation = movementAnimation;
    }

    public override void Init()
    {
        Debug.Log("Entering state: " + GetType());
        m_FSM.Owner.GetComponentInChildren<Animator>().Play(m_MovementAnimation);
    }
    public override void Exit()
    {
        base.Exit();
    }
    public override void FixedUpdate()
    {
        CheckMovementInput();

        //m_RigidBody.AddForce(m_MovimentFinal.normalized * m_MovementSpeed *3, ForceMode.Force);
        m_RigidBody.velocity = m_MovimentFinal.normalized * m_PlayerBehaviour.moveSpeed + new Vector3(0, m_RigidBody.velocity.y, 0);

        if (m_PlayerBehaviour.grounded && m_RigidBody.velocity.y > -0.1f)
        {
            //velocity.y<-0.1f para prevenir que no quede en falling pillado
            if (m_MovimentFinal.x != 0 || m_MovimentFinal.y != 0)
                m_FSM.ChangeState<Walk>();
            else
                m_FSM.ChangeState<Idle>();
        }
            
        

    }
}
