using FiniteStateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Idle : State
{
    AGUSMOVEMENT m_PlayerBehaviour;
    string m_Animation;
    public Idle(FSM fsm,string animation):base(fsm)
    {
        m_Animation = animation;
        m_PlayerBehaviour = m_FSM.Owner.GetComponent<AGUSMOVEMENT>();
    }

    public override void Init()
    {
        base.Init();
        m_FSM.Owner.GetComponentInChildren<Animator>().Play(m_Animation);

        m_PlayerBehaviour.m_InputMap.Land.Jump.performed += StartedJumping;
        m_PlayerBehaviour.m_InputMap.Land.Movement.performed += StartedMoving;
        m_PlayerBehaviour.m_InputMap.Land.Dance1.performed += Dance1;
    }
    public override void Exit()
    {
        base.Exit();
        m_PlayerBehaviour.m_InputMap.Land.Jump.performed -= StartedJumping;
        m_PlayerBehaviour.m_InputMap.Land.Movement.performed -= StartedMoving;
        m_PlayerBehaviour.m_InputMap.Land.Dance1.performed -= Dance1;
    }


    public override void Update()
    {
        base.Update();
        if (!m_PlayerBehaviour.grounded && m_PlayerBehaviour.GetComponent<Rigidbody>().velocity.y<0)
        {
            m_FSM.ChangeState<Falling>();
            Debug.Log("IDLE STATE SAYS: NOT GROUNDED CHANGING TO FALLING");
        }
    }
    private void StartedMoving(InputAction.CallbackContext context)
    {
        if (m_PlayerBehaviour.grounded)
            m_FSM.ChangeState<Walk>();
    }
    private void StartedJumping(InputAction.CallbackContext context)
    {
        if(m_PlayerBehaviour.readyToJump && m_PlayerBehaviour.grounded)
            m_FSM.ChangeState<Jump>();
    }

    private void Dance1(InputAction.CallbackContext context)
    {
        m_FSM.Owner.GetComponentInChildren<Animator>().Play("gangnam");
        SoundManager.PlaySound("gangnamsound");
    }



}
