using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CanvasManagement : MonoBehaviour
{
    //Simplemente borra el canvas "Layout XBOX" al presionar la tecla de salto.
    [SerializeField]
    AGUSMOVEMENT m_PlayerBehaviour;
    private void Start()
    {
        m_PlayerBehaviour.m_InputMap.Land.Jump.performed += StartedJumping2;
    }

    private void StartedJumping2(InputAction.CallbackContext context)
    {
        Debug.Log("BORRO CANVAS XBOX");
        Destroy(this.gameObject);
        m_PlayerBehaviour.m_InputMap.Land.Jump.performed -= StartedJumping2;
    }


}
